# planradar-assessment

![Demo](planradar-assessment.png)

See the [Demo](https://planradar-assessment-zyxneo-4b65a68e1a3001e7ac3f55a2ba7203834da.gitlab.io/)

## Getting started

Set up the proper Node version with [NVM](https://github.com/nvm-sh/nvm)

```bash
nvm use
```

Install the dependencies

```bash
npm i
```

Run the development mode and visit the page on [localhost](http://localhost:8080/)

```bash
npm start
```

Build the project

```bash
npm run build
```

Run the Cypress `end to end` test (While the development mode is running)

```bash
npm run cypress:open
```

Run the command line test

```bash
npm run test
```

## Comparison of the two solutions

The first solution is based on a ChatGPT hallucination.

**Pros**:

- It seems like it is actually scrolled.

**Cons**:

- No matter how the rows are rendered, because the the code in the `useEffect` runs only after the scroll already happened, and that is why empty lines are shown on heavy scroll, it flickers. Rendering more rows does not seem to be fix this issue.
- The task was about 10000+ items, and browsers has limitation on the scroll height.

I slept on it, and came up with a different plan.

**Pros**:

- It only renders rows that are actually visible.
- The scrollbar on the side makes it more versatile and unified across browsers.

**Cons**:

- It may not fully work on some touch devices as expected, but this issue is not proven. 
- It does not seems like it is actually scrolled.

### Possible improvements

- [ ] Add more "off-canvas" rows to the first example in order to reduce flickering
- [x] Add scrollbar to the second example
- [ ] Extract `virtualized` and `row-rendering` logic into custom hooks
- [ ] Make it work with keyboard, up-down arrows...
- [ ] Mix the two solutions in order to improve the cons. Either add scroll animation to the second one, or render them based on "feature-queries"
- [ ] Add loader, sorting etc...
- [ ] Make the height of the table and items dynamic, if possible.
- [ ] Load data dynamically via API

### Dev diary

- 62dd250 - The setup contains general linters that I prefer to use. This project was build from scratch, so there are new version of dependencies, and possible conflicts, that I don't know about at this stage. The rules are very strict, in some cases unnecessarily, these I can turn off later. Setting up this environment and coding according to this rules slows me down significantly, but helps me avoiding some mistakes and errors.
- c19dab7 - Setting up the GitLab pages mimics a more complex CD/CI pipeline. Having it in place at the beginning of the project helps me avoiding further mistakes, and it proves that the project is intact, reproducible in other environments and can be delivered.
- The setup seems extremely strict, and does not fit to this prototyping, but it was a good lesson. The style linter does not work with my vscode, it is probably because of my settings.
- I found this [pages](https://bvaughn.github.io/forward-js-2017/#/12/4) too late, but it is interesting.

### Better done, than tested

[//]: # (This works only on github... And it is just for fun, if I will not be able to finish the testing...)

<picture>
  <source media="(prefers-color-scheme: dark)" srcset="[./theme-dark.svg](https://quotefancy.com/media/wallpaper/3840x2160/4858931-Kent-Beck-Quote-If-testing-costs-more-than-not-testing-then-don-t.jpg)">
  <img alt="Kent Beck quote if testing costs more than not testing then don t" src="https://quotefancy.com/media/wallpaper/3840x2160/1414754-Kent-Beck-Quote-If-testing-costs-more-than-not-testing-then-don-t.jpg">
</picture>