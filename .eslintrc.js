module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "plugin:react/recommended",
        "standard-with-typescript",
        "plugin:prettier/recommended",
        "prettier"
    ],
    "ignorePatterns": [
      ".eslintrc.js",
      "index.html",
      "webpack.config.js",
      "cypress.config.ts",
      "cypress/support/component-index.html",
      "cypress/support/component.ts"
    ],
    "overrides": [
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module",
        "project": "./tsconfig.json",
        "tsconfigRootDir": __dirname,
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "@typescript-eslint/consistent-type-imports": 0,
    },
    "settings": {
        "react": {
          "version": "detect", // React version. "detect" automatically picks the version you have installed.
        },
    },
}
