// eslint-disable-next-line
/// <reference types="cypress" />

describe('Virtualized', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/');
  });

  it('renders properly', () => {
    cy.get('.table').should('exist');
    cy.get('.table__head')
      .find('.table__cell')
      .eq(0)
      .should('have.text', 'index');
  });
});
