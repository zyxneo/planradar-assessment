import React, { ReactNode } from 'react';
import { createRoot } from 'react-dom/client';

import './index.css';
import data from './db/data.json';
import { Table1, Table2 } from './components';

export enum ColumnName {
  ID = 'index',
  Subject = 'subject',
  Progress = 'progress',
  CreatedAt = 'createdAt',
  LastEdit = 'updatedAt',
  Author = 'author',
}

export const headingKeys = {
  [ColumnName.ID]: ColumnName.ID,
  [ColumnName.Subject]: ColumnName.Subject,
  [ColumnName.Progress]: ColumnName.Progress,
  [ColumnName.CreatedAt]: ColumnName.CreatedAt,
  [ColumnName.LastEdit]: ColumnName.LastEdit,
  [ColumnName.Author]: ColumnName.Author,
};

export const columnSizes = {
  [ColumnName.ID]: { flexBasis: 10 },
  [ColumnName.Subject]: { flexBasis: 160 },
  [ColumnName.Progress]: { flexBasis: 30 },
  [ColumnName.CreatedAt]: { flexBasis: 80 },
  [ColumnName.LastEdit]: { flexBasis: 80 },
  [ColumnName.Author]: { flexBasis: 160 },
} as const;

const cellRenderers = {
  [ColumnName.ID]: (value: string | number): ReactNode => (
    <span className="badge badge--danger">{value}</span>
  ),
  [ColumnName.Subject]: (value: string | number): ReactNode => (
    <span className="ellipsis" title={value.toString()}>
      {value}
    </span>
  ),
  [ColumnName.Progress]: (value: string | number): ReactNode => value,
  [ColumnName.CreatedAt]: (value: string | number): ReactNode => (
    <span className="ellipsis" title={value.toString()}>
      {new Date(value).toLocaleDateString('de-DE')}
    </span>
  ),
  [ColumnName.LastEdit]: (value: string | number): ReactNode => {
    const formattedDateTime = `${new Date(value).toLocaleTimeString('de-DE')} ${new Date(value).toLocaleDateString('de-DE')}`;
    return (
      <span className="ellipsis" title={formattedDateTime}>
        {formattedDateTime}
      </span>
    );
  },
  [ColumnName.Author]: (value: string | number): ReactNode => (
    <span className="ellipsis" title={value.toString()}>
      {value}
    </span>
  ),
};

const container = document.querySelector('.app');

const renderToDOM = (): ReactNode => {
  if (container !== null) {
    const root = createRoot(container);
    root.render(
      <div className="demo">
        <h1 className="demo__title">Assessment Frontend Developer</h1>
        <div className="demo__table">
          <h2>Solution 1</h2>
          <p>The first solution is based on a ChatGPT hallucination.</p>
          <p>
            <strong>Pros</strong>:
          </p>
          <ul>
            <li>It seems like it is actually scrolled.</li>
          </ul>
          <p>
            <strong>Cons</strong>:
          </p>
          <ul>
            <li>
              No matter how the rows are rendered, because the the code in the{' '}
              <code>useEffect</code> runs only after the scroll already
              happened, and that is why empty lines are shown on heavy scroll,
              it flickers. Rendering more rows does not seem to be fix this
              issue.
            </li>
            <li>
              The task was about 10000+ items, and browsers has limitation on
              the scroll height.
            </li>
          </ul>
          <Table1
            data={data}
            height={200}
            itemHeight={50}
            columnSizes={columnSizes}
            cellRenderers={cellRenderers}
          />
        </div>
        <div className="demo__table">
          <h2>Solution 2</h2>
          <p>
            <strong>Pros</strong>:
          </p>
          <ul>
            <li>It only renders rows that are actually visible.</li>
            <li>
              The scrollbar on the side makes it more versatile and unified
              across browsers.
            </li>
          </ul>
          <p>
            <strong>Cons</strong>:
          </p>
          <ul>
            <li>
              It may not fully work on some touch devices as expected, but this
              issue is not proven.{' '}
            </li>
            <li>It does not seems like it is actually scrolled.</li>
          </ul>
          <Table2
            data={data}
            height={200}
            itemHeight={50}
            columnSizes={columnSizes}
            cellRenderers={cellRenderers}
          />
        </div>
        <div className="demo__footer">
          <a href="https://gitlab.com/zyxneo/planradar-assessment/">
            Source on GitLab
          </a>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

renderToDOM();

export { renderToDOM };
