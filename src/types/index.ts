export interface TicketData {
  [key: string]: string | number;
  _id: string;
  index: number;
  author: string;
  createdAt: string;
  progress: number;
}

export type ColumnSizes = Record<string, Record<string, string | number>>;
