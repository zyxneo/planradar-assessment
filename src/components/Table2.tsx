import React, { ReactNode, useEffect, useRef, useState } from 'react';

import { TableRow } from '.';
import { ColumnSizes, TicketData } from '../types';

interface Props {
  data: TicketData[];
  height: number;
  itemHeight: number;
  columnSizes: ColumnSizes;
  cellRenderers: Record<string, (value: string | number) => ReactNode>;
}

const VirtualizedTable2 = ({
  data,
  height,
  itemHeight,
  columnSizes,
  cellRenderers,
}: Props): ReactNode => {
  const [startIndex, setStartIndex] = useState(0);
  const [visibleItems, setVisibleItems] = useState(data);
  const containerRef = useRef(null as HTMLDivElement | null);
  const scrollbarRef = useRef(null as HTMLDivElement | null);
  const handlerRef = useRef(null as HTMLDivElement | null);
  const [scrollHandlerY, setScrollHandlerY] = useState(0);

  useEffect(() => {
    const container = containerRef.current;

    let topIndex = startIndex;
    let deltaY = 0;
    const handleScroll = (event: WheelEvent): void => {
      deltaY = event.deltaY;

      const visibleItems = Math.max(Math.ceil(height / itemHeight));
      if (deltaY >= 0) {
        console.info('Scrolling down by wheel');
        topIndex = Math.min(topIndex + 1, data.length - visibleItems);
      } else {
        console.info('Scrolling up by wheel');
        topIndex = Math.max(topIndex - 1, 0);
      }
      const bottomIndex = topIndex + visibleItems;

      const itemsToBeRendered = data.slice(topIndex, bottomIndex);

      setVisibleItems(itemsToBeRendered);

      // Move the scrollbar to the proper position.
      if (scrollbarRef.current !== null && handlerRef.current !== null) {
        const scrollbarRect = scrollbarRef.current.getBoundingClientRect();
        const handlerRect = handlerRef.current.getBoundingClientRect();

        const scrollbarTravelDistance =
          scrollbarRect.height - handlerRect.height;
        const scrollRatio = topIndex / (data.length - visibleItems);
        setScrollHandlerY(scrollbarTravelDistance * scrollRatio);
      }
    };

    // @ts-expect-error - Set initial scroll in order to render the proper amount of items
    handleScroll({ deltaY: 0 });

    // Block the window scroll while the table is scrolled. `event.preventDefault()` does not work unfortunately
    const handleMouseenter = (): void => {
      document.body.style.overflow = 'hidden';
    };
    const handleMouseleave = (): void => {
      document.body.style.overflow = 'inherit';
    };

    container?.addEventListener('mouseenter', handleMouseenter, false);
    container?.addEventListener('mouseleave', handleMouseleave, false);
    container?.addEventListener('wheel', handleScroll, false);

    return () => {
      container?.removeEventListener('mouseenter', handleMouseenter);
      container?.removeEventListener('mouseleave', handleMouseleave);
      container?.removeEventListener('wheel', handleScroll);
    };
  }, [startIndex]);

  const handleMouseDown = (event: MouseEvent): void => {
    if (scrollbarRef.current !== null && handlerRef.current !== null) {
      // https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
      const scrollbarRect = scrollbarRef.current.getBoundingClientRect();
      const handlerRect = handlerRef.current.getBoundingClientRect();

      // do not scroll lower than the bottom of the scrollbar
      const maxScrollDownY = scrollbarRect.height - handlerRect.height;

      // Calculate initial offset of mouse pointer relative to handler position
      const initialMouseOffset =
        event.clientY - handlerRect.y + handlerRef.current.offsetTop;

      const initialHandlerOffset = handlerRef.current.offsetTop;

      // Event listener for mousemove while dragging
      const handleMouseMove = (event: MouseEvent): void => {
        if (scrollbarRef.current !== null && handlerRef.current !== null) {
          const calculatedHandlerOffset =
            initialHandlerOffset -
            initialMouseOffset +
            (event.clientY - scrollbarRect.y);

          // Restrict the movement boundaries of the handler into the scrollbar
          const newScrollHandlerY = Math.min(
            Math.max(calculatedHandlerOffset, 0),
            maxScrollDownY
          );
          setScrollHandlerY(newScrollHandlerY);

          const topIndex =
            (newScrollHandlerY / maxScrollDownY) * (data.length - 4);
          const bottomIndex = topIndex + 4;
          const itemsToBeRendered = data.slice(topIndex, bottomIndex);

          setStartIndex(topIndex);
          setVisibleItems(itemsToBeRendered);
        }
      };

      // Event listener for mouseup to stop dragging
      const handleMouseUp = (): void => {
        // Remove event listeners on mouseup
        window.removeEventListener('mousemove', handleMouseMove);
        window.removeEventListener('mouseup', handleMouseUp);
      };
      // Add event listeners for mousemove and mouseup
      window.addEventListener('mousemove', handleMouseMove);
      window.addEventListener('mouseup', handleMouseUp);
    }
  };

  return (
    <div className="table">
      <div className="table__head">
        <div className="table__row">
          {Object.keys(columnSizes).map(function (key, i) {
            return (
              <div
                key={key}
                style={{ ...columnSizes[key], height: itemHeight }}
                className="table__cell"
              >
                {key}
              </div>
            );
          })}
        </div>
      </div>
      <div ref={containerRef} className="table__body" style={{ height }}>
        <div className="virtualizedWrapper">
          {visibleItems.map((item, index) => {
            return (
              <TableRow
                key={item._id}
                data={item}
                columnSizes={columnSizes}
                cellRenderers={cellRenderers}
                height={itemHeight}
                style={{ position: 'relative' }}
              />
            );
          })}
        </div>
        <div ref={scrollbarRef} className="scrollbar">
          <div
            ref={handlerRef}
            className="scrollbar__handler"
            // @ts-expect-error - TODO - find the correct event type
            onMouseDown={handleMouseDown}
            style={{ top: scrollHandlerY }}
            role="scrollbar"
            tabIndex={0}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default VirtualizedTable2;
