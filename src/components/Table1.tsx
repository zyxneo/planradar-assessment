import React, { ReactNode, useEffect, useRef, useState } from 'react';

import { TableRow } from '.';
import { ColumnSizes, TicketData } from '../types';

interface Props {
  data: TicketData[];
  height: number;
  itemHeight: number;
  columnSizes: ColumnSizes;
  cellRenderers: Record<string, (value: string | number) => ReactNode>;
}

const VirtualizedTable1 = ({
  data,
  height,
  itemHeight,
  columnSizes,
  cellRenderers,
}: Props): ReactNode => {
  const [scrollTop, setScrollTop] = useState(0);
  const [startIndex, setStartIndex] = useState(0);
  const [visibleItems, setVisibleItems] = useState(data);
  const containerRef = useRef(null as HTMLDivElement | null);

  useEffect(() => {
    const container = containerRef.current;

    const handleScroll = (event: Event): void => {
      const newScrollTop =
        container?.scrollTop !== null ? Number(container?.scrollTop) : 0;
      setScrollTop(newScrollTop);
    };

    container?.addEventListener('scroll', handleScroll);

    // Scroll to the top on refreshing the page, otherwise the Items can be out of the view, because of the kept scroll position of the browser
    container?.scrollTo(0, 0);

    return () => {
      container?.removeEventListener('scroll', handleScroll);
    };
  }, []);

  useEffect(() => {
    const itemsScrollTop = startIndex * itemHeight;

    const diff = scrollTop - itemsScrollTop;

    const visibleItems = Math.max(
      Math.ceil((height + diff) / itemHeight) + 1,
      Math.ceil(height / itemHeight) + 1
    );

    const topIndex = Math.max(0, Math.floor(scrollTop / itemHeight));
    const bottomIndex = topIndex + visibleItems;

    const itemsToBeRendered = data.slice(topIndex, bottomIndex);

    setStartIndex(topIndex);
    setVisibleItems(itemsToBeRendered);
  }, [scrollTop]);

  return (
    <div className="table">
      <div className="table__head">
        <div className="table__row">
          {Object.keys(columnSizes).map(function (key, i) {
            return (
              <div
                key={key}
                style={{ ...columnSizes[key], height: itemHeight }}
                className="table__cell"
              >
                {key}
              </div>
            );
          })}
        </div>
      </div>
      <div ref={containerRef} className="table__body" style={{ height }}>
        <div
          className="virtualizedWrapper"
          style={{ height: data.length * itemHeight }}
        >
          {visibleItems.map((item, index) => {
            return (
              <TableRow
                key={item._id}
                data={item}
                style={{ top: (startIndex + index) * itemHeight }}
                columnSizes={columnSizes}
                cellRenderers={cellRenderers}
                height={itemHeight}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default VirtualizedTable1;
