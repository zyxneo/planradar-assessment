import React, { ReactNode } from 'react';

import { ColumnSizes, TicketData } from '../types';

interface Props {
  data: TicketData;
  style?: Record<string, unknown>;
  height: number;
  columnSizes: ColumnSizes;
  cellRenderers: Record<string, (value: string | number) => ReactNode>;
}

const TableRow = ({
  data,
  style,
  columnSizes,
  cellRenderers,
  height,
}: Props): ReactNode => {
  const renderCells = Object.keys(columnSizes).map(function (key, i) {
    return (
      <div key={key} style={{ ...columnSizes[key] }} className="table__cell">
        {cellRenderers[key](data[key])}
      </div>
    );
  });

  return (
    <div style={{ ...style, height }} className="table__row">
      {renderCells}
    </div>
  );
};

export default TableRow;
