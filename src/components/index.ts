import Table1 from './Table1';
import Table2 from './Table2';
import TableRow from './TableRow';

export { Table1, Table2, TableRow };
