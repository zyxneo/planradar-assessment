# Template for data generation

[json-generator.com](https://json-generator.com/)

```
[
  '{{repeat(1, 1000)}}',
  {
    _id: '{{objectId()}}',
    type: "tickets",
    index: '{{index()}}',
    uuid: '{{guid()}}',
    author: '{{firstName()}} {{surname()}}',
    componentId: '{{guid()}}',
    ticketTypeTd: '{{guid()}}',
    customerTd: '{{guid()}}',
    serverUpdatedAt: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-ddThh:mm:ss Z")}}',
    createdAt: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-ddThh:mm:ss Z")}}',
    updatedAt: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-ddThh:mm:ss Z")}}',
    subject: '{{lorem(3, "words")}}',
    progress: '{{integer(0, 4)}}',
    projectId: '{{guid()}}',
  }
]
```

based on `https://www.planradar.com/api/v2/1271518/projects/1271519/tickets?page=1&pagesize=100&component_id=gqxemob&hide_closed_tickets=true&sort=sequential_id&group_by_parent=true&include_component_ticket_count=true`
